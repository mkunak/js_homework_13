// Задание:
// Реализовать возможность смены цветовой темы сайта пользователем.
//
// Технические требования:
// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение.
// При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы
//
// Литература:
// LocalStorage на пальцах

console.log(` -------------------- JS_Homework#13. LocalStorage. Change color theme -------------------- `);

document.addEventListener('DOMContentLoaded', function () {
    const btn = document.getElementById(`btnOfChange`);
    let styleCSS = document.getElementsByTagName("link")[4];
    console.log(styleCSS.getAttribute(`href`));
    if (localStorage.getItem(`switchStyle`) !== null) {
        styleCSS.setAttribute(`href`, localStorage.getItem(`switchStyle`));
    }
    btn.addEventListener(`click`, function () {
        if (styleCSS.getAttribute(`href`) === `css/style.css`) {
            styleCSS.setAttribute(`href`, `css/style-new.css`);
        } else {
            styleCSS.setAttribute(`href`, `css/style.css`);
        }
        localStorage.setItem(`switchStyle`, styleCSS.getAttribute(`href`));
    });
});